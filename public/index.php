<?php
/**
 * Created by PhpStorm.
 * User: ddis
 * Date: 06.01.2018
 * Time: 11:11
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\{Application, View};

try
{
    //Register an autoloader
    $loader = new Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/'
    ))->register();

    //Create a DI
    $di = new FactoryDefault();

    //Setting up the view component
    $di->set('view', function(){
        $view = new View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    //Handle the request
    $application = new Application();
    $application->setDI($di);
    echo $application->handle()->getContent();

}
catch(\Phalcon\Exception $e)
{
    echo "PhalconException: ", $e->getMessage();
}