<?php
/**
 * Created by PhpStorm.
 * User: ddis
 * Date: 06.01.2018
 * Time: 11:17
 */

class IndexController extends \Phalcon\Mvc\Controller
{
    /**
     * @throws Exception
     */
    public function initialize()
    {

    }
    
    public function indexAction()
    {
        $this->view->setVar('name', 'ddis');
        $this->view->setVars([
            'name' => 'Dmitry',
            'lastName' => 'Sokolov'
        ]);
    }

    public function jsonAction()
    {
        $this->view->setVar('name', 'ddis');
        $this->view->setVars([
            'name' => 'Dmitry',
            'lastName' => 'Sokolov'
        ]);

        $json = [
            'name' => 'ddis',
            'lastName' => 'pinfloid',
        ];

        return json_encode($json);
    }
}